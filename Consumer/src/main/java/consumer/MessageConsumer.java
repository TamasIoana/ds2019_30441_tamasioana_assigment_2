package consumer;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.apache.log4j.BasicConfigurator;
import org.json.JSONException;
import org.json.JSONObject;

import javax.xml.bind.DatatypeConverter;
import javax.xml.crypto.Data;
import java.io.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Scanner;


public class MessageConsumer {
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/medication";
    private static final String USER = "root";
    private static final String PASS = "rihanna";


    private final static String QUEUE_NAME = "logs";

    public static void main(String[] argv) throws Exception {
        BasicConfigurator.configure();

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();



        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {

            String message = new String(delivery.getBody(), "UTF-8");
        //    System.out.println(activity);
            JSONObject myJsonObj = null;
            String activity = null;
            String id=null;
            String start=null;
            String end=null;

            try {
                myJsonObj = new JSONObject(message);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                activity = myJsonObj.getString("activity");
                id=myJsonObj.getString("patient_id");
                start=myJsonObj.getString("start_time");
                end=myJsonObj.getString("end_time");
                Long startlong = Long.valueOf(start);
                Long endlong = Long.valueOf(end);

                if(activity.equals("Sleeping")){
                        if((((endlong%1000000)/10000)*3600+((endlong%10000)/100)*60+ endlong%100 )- (((startlong%1000000)/10000)*3600+((startlong%10000)/100)*60+ startlong%100 ) > 12*3600){
                            System.out.println(" RULE 1 ");
                        }
                }else if(activity.equals("Leaving")){
                    if((((endlong%1000000)/10000)*3600+((endlong%10000)/100)*60+ endlong%100 )- (((startlong%1000000)/10000)*3600+((startlong%10000)/100)*60+ startlong%100 ) > 12*3600){
                        System.out.println(" RULE 2 ");
                    }
                }else if(activity.equals("Toileting")){ if((((endlong%1000000)/10000)*3600+((endlong%10000)/100)*60+ endlong%100 )- (((startlong%1000000)/10000)*3600+((startlong%10000)/100)*60+ startlong%100 ) > 10*3600){
                    System.out.println(" RULE 3 ");
                }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

         try{

             java.sql.Connection conn= DriverManager.getConnection(DBURL, USER, PASS);

             String query = " insert into patient_sensor (patient_id, activity,start_time,end_time) values (?, ?,?,?)";

             PreparedStatement preparedStmt = conn.prepareStatement(query);
             preparedStmt.setInt (1, Integer.parseInt(id));
             preparedStmt.setString (2, activity);
             preparedStmt.setString (3, start);
             preparedStmt.setString (4, end);


             preparedStmt.execute();

             conn.close();

         }catch(Exception e){
             System.err.println("Got an exception!");
             System.err.println(e.getMessage());

         }
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });



    }


}
