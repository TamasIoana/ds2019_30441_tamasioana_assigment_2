package producer;

public class ModelData {
    String patient_id;
    String activity;
    String start_time;
    String end_time;


    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public ModelData(String field, String field3 , String field1, String field2) {
        this.patient_id=field;
        this.activity=field3;
        this.start_time=field1;
        this.end_time=field2;

    }




}