package producer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;


import java.io.*;

public class MessageProducer {
    private static final String QUEUE_NAME = "logs";


    public static void main(String[] argv) throws Exception  {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) { //create connection to the server
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            File file = new File("activity.txt");
            BufferedReader reader;
            try {
                reader = new BufferedReader(new FileReader(file));
                String line;
                String[] fields;
                while ((line = reader.readLine()) != null) {
                    fields = line.trim().split("\\t+");//remove spaces and split by tabs

                        String str= fields[0];
                        String strNew = str.replace("-", "");
                        String strNew1=strNew.replace(" ","");
                        String strNew2=strNew1.replace(":","");

                        String str2= fields[1];
                        String strNew3 = str2.replace("-", "");
                        String strNew4=strNew3.replace(" ","");
                        String strNew5=strNew4.replace(":","");

                    ModelData data =new ModelData("1", fields[2], strNew2, strNew5);

                    // Creating Object of ObjectMapper define in Jakson Api
                    ObjectMapper Obj = new ObjectMapper();
                    String jsonStr = Obj.writeValueAsString(data);

                    channel.basicPublish("", QUEUE_NAME, null, jsonStr.getBytes("UTF-8"));
                    //System.out.println("message sent");
                    System.out.println(jsonStr);
                    //  Thread.sleep(200);
                }

            } catch (FileNotFoundException e) {
                System.out.println("File not found!!!!");

            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("File not found!");
            }


        }
    }

}